package com.surya.interview.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.surya.interview.pojo.Details;

import java.util.ArrayList;

/**
 * Created by Mamatha Gowda on 2/27/2019.
 * Alcotech Internet Private Ltd.
 */
public class DatabaseHelper  extends SQLiteOpenHelper {

    // Database Name
    public static final String DATABASE_NAME = "surya";
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Table Name
    private static final String TABLE_VIPS = "VIPS";

    private static final String CREATE_TABLE_VIPS = "CREATE TABLE "
            + TABLE_VIPS + "(" + "id" + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "firstName" + " TEXT,"  + "lastName" + " TEXT,"
            + "emailId" + " TEXT," + "imageUrl" + " TEXT" + ")";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.e("sqlite","database created");
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_VIPS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VIPS + ";");
        onCreate(db);
    }

    // insert the Item values
    public long insertItem(Details.Item item) {
        long item_id = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        String sql = "SELECT name FROM sqlite_master WHERE type='table' AND name='"+TABLE_VIPS+"'";
        try {
            cursor = db.rawQuery(sql, null);
            if (cursor.getCount() > 0) {
                ContentValues values = new ContentValues();
                values.put("firstName", item.getFirstName());
                values.put("lastName", item.getLastName());
                values.put("emailId", item.getEmailId());
                values.put("imageUrl", item.getImageUrl());

                // insert row
                item_id = db.insert(TABLE_VIPS, null, values);
                Log.d("sqlite", " value inserted id " + item_id);
            }else {
                Log.e("sqlite"," table not created");
                onCreate(db);
            }
        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }
        return item_id;
    }

    // get all items values
    public ArrayList<Details.Item> getItemlist(){
        ArrayList<Details.Item> itemArrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        String sql = "SELECT name FROM sqlite_master WHERE type='table' AND name='"+TABLE_VIPS+"'";

        cursor = db.rawQuery(sql, null);
        if (cursor.getCount() > 0) {
            String selectQuery = "SELECT  * FROM " + TABLE_VIPS;
            Cursor c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    Details.Item item  = new Details.Item();
                    item.setFirstName(c.getString(c.getColumnIndex("firstName")));
                    item.setLastName(c.getString(c.getColumnIndex("lastName")));
                    item.setEmailId(c.getString(c.getColumnIndex("emailId")));
                    item.setImageUrl(c.getString(c.getColumnIndex("imageUrl")));

                    itemArrayList.add(item);
                } while (c.moveToNext());
            }

        }else {
            Log.e("sqlite"," table not created");
        }
        return itemArrayList;
    }

    // delete(clear) the VIPS table
    public void removeItems(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        String sql = "SELECT name FROM sqlite_master WHERE type='table' AND name='"+TABLE_VIPS+"'";

        cursor = db.rawQuery(sql, null);
        if (cursor.getCount() > 0) {
            db.execSQL("delete from "+ TABLE_VIPS);
        }else {
            Log.e("sqlite"," table not created");
        }
    }
}
