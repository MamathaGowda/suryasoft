package com.surya.interview;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.SocketTimeoutException;

import okhttp3.ResponseBody;
import retrofit2.HttpException;

/**
 * Created by Mamatha Gowda on 2/27/2019.
 * Alcotech Internet Private Ltd.
 */
public class Common {
    private static WeakReference<CommonView> weakReference;
    public static void error(CommonView view1, Throwable e){
        weakReference = new WeakReference<>(view1);
        CommonView view = weakReference.get();
        if (e instanceof HttpException) {
            ResponseBody responseBody = ((HttpException)e).response().errorBody();
            if(e.getMessage()!=null)
                view.onUnknownError(e.getMessage(),"");
        } else if (e instanceof SocketTimeoutException) {
            view.onTimeout();
        } else if (e instanceof IOException) {

        } else {
            if(e.getMessage()!=null)
                view.onUnknownError(e.getMessage(),"");
        }
    }

    // set the layout mgr to recycler view
    public static void rv(Context context, int orentation, RecyclerView rv){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        if(orentation==1){
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        }else {
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        }
        rv.setLayoutManager(linearLayoutManager);
    }

    // dislay image using glide library
    public static void displayimage(Context context, String url, ImageView img){
        RequestOptions options = new RequestOptions()
               // .placeholder(R.drawable.broken_image)
                .error(R.drawable.broken_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        Glide.with(context)
                .load(url)
                .apply(options)
                .into(img);
    }
}
