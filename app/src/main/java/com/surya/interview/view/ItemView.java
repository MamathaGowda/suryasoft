package com.surya.interview.view;

import com.surya.interview.CommonView;
import com.surya.interview.pojo.Details;

import java.util.ArrayList;

/**
 * Created by Mamatha Gowda on 2/27/2019.
 * Alcotech Internet Private Ltd.
 */
public interface ItemView {
    interface View extends CommonView {
        void init();
        void setAdapter();
        void insertitemstoSqlite(ArrayList<Details.Item> itemArrayList);
    }
}
