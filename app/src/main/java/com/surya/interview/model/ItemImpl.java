package com.surya.interview.model;

import android.content.Context;

import com.google.gson.JsonObject;
import com.surya.interview.Common;
import com.surya.interview.ItemsActivity;
import com.surya.interview.pojo.Details;
import com.surya.interview.pojo.Email;
import com.surya.interview.presenter.ItemPresenter;
import com.surya.interview.retrofit2.RestApi;
import com.surya.interview.view.ItemView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mamatha Gowda on 2/27/2019.
 * Alcotech Internet Private Ltd.
 */
public class ItemImpl implements ItemPresenter{
    RestApi restApi;
    ItemView.View view;

    public ItemImpl(RestApi restApi, ItemView.View view) {
        this.restApi = restApi;
        this.view = view;
    }


    @Override
    public void getItems(Context context, RestApi restApi, String emailid) {
        //converting string to json
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("emailId",emailid);
        Call<Details> call = restApi.postItems(jsonObject);
        call.enqueue(new Callback<Details>() {
            @Override
            public void onResponse(Call<Details> call, Response<Details> response) {
                Details resource = response.body();
                ArrayList<Details.Item> itemArrayList  = new ArrayList<>();
                itemArrayList = resource.getItems();
                view.insertitemstoSqlite(itemArrayList);
                }

            @Override
            public void onFailure(Call<Details> call, Throwable t) {
                Common.error(view,t);
            }
        });
    }
}
