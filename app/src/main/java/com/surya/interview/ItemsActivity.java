package com.surya.interview;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.surya.interview.model.ItemImpl;
import com.surya.interview.pojo.Details;
import com.surya.interview.presenter.ItemPresenter;
import com.surya.interview.retrofit2.APIClient;
import com.surya.interview.retrofit2.RestApi;
import com.surya.interview.sqlite.DatabaseHelper;
import com.surya.interview.view.ItemView;

import java.util.ArrayList;

public class ItemsActivity extends AppCompatActivity implements ItemView.View{
    DatabaseHelper db;
    RestApi restApi;
    ItemPresenter mitemPresenter;
    RecyclerView mrvItems;
    private EmailValidator mEmailValidator;
    String email="john@doe.com";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        init();
        ArrayList<Details.Item> itemArrayList =  db.getItemlist();
        if(itemArrayList.size()>0){
            setAdapter();
        }else {
            mEmailValidator = new EmailValidator();
            boolean valid = mEmailValidator.textvalidation(email);
            if(valid){
                mitemPresenter.getItems(getApplicationContext(),restApi,email);
            }else {
                Toast.makeText(getApplicationContext(),"Please provide the valid email id!",Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void init() {
        mrvItems = findViewById(R.id.rvItems);
        Common.rv(getApplicationContext(),1,mrvItems);
        db = new DatabaseHelper(getApplicationContext());
        restApi = APIClient.getClient().create(RestApi.class);
        mitemPresenter = new ItemImpl(restApi, ItemsActivity.this);
    }

    @Override
    public void setAdapter() {
        ArrayList<Details.Item> itemArrayList =  db.getItemlist();
        mrvItems.setAdapter(new ItemAdapter(itemArrayList));
    }

    @Override
    public void insertitemstoSqlite(ArrayList<Details.Item> itemArrayList) {
        db.removeItems();
        for (int  i=0;i<itemArrayList.size();i++){
            Long id = db.insertItem(itemArrayList.get(i));
        }
        setAdapter();
    }
    @Override
    public void onUnknownError(String message, String error) {

    }

    @Override
    public void onTimeout() {

    }

    @Override
    public void onFailure(String errorMsg) {

    }
    private class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView mimgprofile;
        TextView mtvfname,mtvemail;

        public MyViewHolder(View itemView) {
            super(itemView);
            mtvfname = (TextView) itemView.findViewById(R.id.tvfname);
            mtvemail = (TextView) itemView.findViewById(R.id.tvemail);
            mimgprofile = (ImageView) itemView.findViewById(R.id.imgprofile);
        }
    }
    private class ItemAdapter extends RecyclerView.Adapter<MyViewHolder> {
        ArrayList<Details.Item> itemArrayList;

        public ItemAdapter(ArrayList<Details.Item> itemArrayList) {
            this.itemArrayList = itemArrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, parent, false));
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            holder.mtvfname.setText(itemArrayList.get(position).getFirstName()+" "+itemArrayList.get(position).getLastName());

            holder.mtvemail.setText(itemArrayList.get(position).getEmailId());
            Common.displayimage(getApplicationContext(),itemArrayList.get(position).getImageUrl(),holder.mimgprofile);
        }

        @Override
        public int getItemCount() {
            return itemArrayList==null ? 0 : itemArrayList.size();
        }
    }

}
