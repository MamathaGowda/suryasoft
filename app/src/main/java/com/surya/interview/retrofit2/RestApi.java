package com.surya.interview.retrofit2;

import com.google.gson.JsonObject;
import com.surya.interview.pojo.Details;
import com.surya.interview.pojo.Email;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Mamatha Gowda on 2/27/2019.
 * Alcotech Internet Private Ltd.
 */
public interface RestApi {
    //get item list
    @POST("/list")
    Call<Details> postItems(@Body JsonObject emailId);
}
