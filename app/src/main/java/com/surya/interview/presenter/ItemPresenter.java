package com.surya.interview.presenter;

import android.content.Context;

import com.surya.interview.retrofit2.RestApi;

/**
 * Created by Mamatha Gowda on 2/27/2019.
 * Alcotech Internet Private Ltd.
 */
public interface ItemPresenter {
    void getItems(Context context, RestApi restApi,String emailid);
}
