package com.surya.interview;

public interface CommonView {
    void onUnknownError(String message, String error);
    void onTimeout();
    void onFailure(String errorMsg);
}
